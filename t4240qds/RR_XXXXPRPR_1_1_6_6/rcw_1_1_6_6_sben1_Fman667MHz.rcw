/*
 * T4240QDS RCW for SerDes[1:4] Protocol 1, 1, 6, 6
 *
 * This RCW is for better performance, it set FMAN at 667MHz, and
 * FMAN:MAC to 2:1, override VDD to 1.15v is also needed.
 *
 * 42G configuration -- 2 RGMII + 4 XAUI
 *
 * Frequencies:
 *
 * Sys Clock -- 66.67 Mhz
 * SDREFCLK1_FSEL: 125 MHz
 * SDREFCLK2_FSEL: 125 MHz
 * SDREFCLK3_FSEL: 100 MHz
 * SDREFCLK3_FSEL: 100 MHz
 *
 * Core -- 1666 MHz (Mul 25 )
 * Platform - 600 MHz (Mul 9)
 * DDR -- 1600 MHz (Mul 12 of SW_DDRCLK: 133.33MHz)
 * FMAN1 -- 667 MHz (Cluster group B PLL 2)/2
 * FMAN2 -- 667 MHz (Cluster group B PLL 2)/2
 * FMAN:MAC -- 2:1
 *
 * Slot  Card
 * 1     XAUI
 * 2     XAUI
 * 3     XAUI
 * 4     XAUI
 * 5     PCIe1 x4
 * 6     SRIO x4
 * 7     PCIe3 x4
 * 8     SRIO  x4
 *
 * RGMII1: FM1@MAC5
 * RGMII2: FM2@MAC5
 *
 * PBI source is I2C, the RCW also works for the PBI source as MMC or eSPI
 */

#include <t4240.rcwi>

SYS_PLL_RAT=9
MEM_PLL_RAT=12
CGA_PLL1_RAT=25
CGA_PLL2_RAT=12
CGA_PLL3_RAT=16
CGB_PLL1_RAT=25
CGB_PLL2_RAT=20
SRDS_PRTCL_S1=1
SRDS_PRTCL_S2=1
SRDS_PRTCL_S3=6
SRDS_PRTCL_S4=6
SRDS_PLL_PD_S1=1
SRDS_PLL_PD_S2=1
SRDS_PLL_PD_S3=1
SRDS_PLL_PD_S4=1
SRDS_DIV_PEX_S3=2
SRDS_DIV_AURORA_S4=1
SRDS_DIV_PEX_S4=2
PBI_SRC=1
BOOT_LOC=24
BOOT_HO=0
SB_EN=1
IFC_MODE=32
HWA_CGB_M1_CLK_SEL=6
DRAM_LAT=1
GP_INFO=3992977646
UART_BASE=6
IRQ_BASE=511
HWA_CGB_M2_CLK_SEL=2

// IFC bus speed work-around
.pbi
write 0x1241c0, 0xf03f3f3f
write 0x1241c4, 0xff003f3f
write 0x124010, 0x00000101
write 0x124130, 0x0000000c
.end
// SERDES A-006031 work-around
.pbi
write 0x0ea000, 0x064740e6
write 0x0ea020, 0x064740e6
write 0x0eb000, 0x064740e6
write 0x0eb020, 0x064740e6
write 0x0ec000, 0x064740e6
write 0x0ec020, 0x064740e6
write 0x0ed000, 0x064740e6
write 0x0ed020, 0x064740e6
.end

#include <secure_boot_pbi.rcw>
